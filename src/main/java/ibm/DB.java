package ibm;
import java.sql.*;
import java.util.*;

public class DB {

	private final String userName = "root";
	private final String password = "1107";
	private final String serverName = "localhost";
	private final int portNumber = 1107;
	private final String dbName = "test";
	private final String tableName = "PhoneDirectory";
	private Statement stmt;

	public Connection getConnection() throws SQLException {
		/*
		 * Connect database and return connection
		 */
		Connection conn = null;
		Properties connectionProps = new Properties();
		connectionProps.put("user", this.userName);
		connectionProps.put("password", this.password);
		System.out.println("trying to get connection!!! ");
		conn = DriverManager.getConnection("jdbc:mysql://" + this.serverName
				+ ":" + this.portNumber + "/" + this.dbName, connectionProps);
		System.out.println("Connection Achieved!!!");
		return conn;
	}

	public void createTable(Connection conn){
		/*
		 * Create table
		 */
		try {
			stmt = conn.createStatement();
			String createString = "CREATE TABLE " + this.tableName + "(ID INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT, NAME VARCHAR(50) NOT NULL, Number VARCHAR(20))";
			stmt.executeUpdate(createString);
			System.out.println(this.tableName + " table created");
		} catch (SQLException e) {
			System.out.println("Couldn't create a table");
			e.printStackTrace();
			return;
		}
	}

	public void insertTable(Connection conn, PhoneDirectory record){
		/*
		 * Insert record in the table
		 */
		try {
			stmt = conn.createStatement();
			String input = "insert into " + this.tableName + " (NAME, NUMBER) values('" + record.getName() + "', '" + record.getNumber() + "');";
			stmt.executeUpdate(input);
			System.out.println("Record inserted");
		} catch (SQLException e) {
			System.out.println("Couldn't insert the record");
			e.printStackTrace();
			return;
		}
	}

	public void updateTable(Connection conn, PhoneDirectory record){
		/*
		 * Update record in the table based on name
		 */
		try {
			stmt = conn.createStatement();
			String input = "UPDATE " + this.tableName + " SET NUMBER = " + record.getNumber() + " WHERE NAME = '" + record.getName() + "'";
			stmt.executeUpdate(input);
			System.out.println("Record updated");
		} catch (SQLException e) {
			System.out.println("Couldn't update the record");
			e.printStackTrace();
			return;
		}
	}

	public boolean findTable(Connection conn, PhoneDirectory record){
		/*
		 * Find record in the table based on name
		 */
		stmt = null;
		try {
			stmt = conn.createStatement();
			String sql = "SELECT * FROM " + this.tableName + " WHERE NAME = '" + record.getName() + "'";;
			ResultSet rs = stmt.executeQuery(sql);
			return rs.next();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public void dropTable(Connection conn){
		/*
		 * Drop table
		 */
		try {
			stmt = conn.createStatement();
			String dropString = "DROP TABLE " + this.tableName;
			stmt.executeUpdate(dropString);
			System.out.println(this.tableName + " table dropped");
		} catch (SQLException e) {
			System.out.println("Couldn't drop table");
			return;
		}
	}

	public ResultSet SelectAll(Connection conn){
		/*
		 * Retrieve all the records from the table
		 */
		ResultSet rs = null;
		try {
			stmt = conn.createStatement();
			String sql = "SELECT * FROM " + this.tableName;
			rs = stmt.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}
	
	public int getCount(Connection conn){
		/*
		 * Get total number of records from the table
		 */
		try {
			stmt = conn.createStatement();
			String sql = "SELECT COUNT(*) FROM " + this.tableName;
			ResultSet rs = stmt.executeQuery(sql);
			rs.next();
			return rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public static void main(String[] args) {
		/*
		 * Function to test the integration with DB directly
		 */
		DB obj = new DB();

		try {
			Connection conn = obj.getConnection();
			obj.dropTable(conn);
			obj.createTable(conn);
			obj.insertTable(conn, new PhoneDirectory("test", "123"));
			obj.SelectAll(conn);
			System.out.println(obj.findTable(conn, new PhoneDirectory("test", "1234")));
			obj.updateTable(conn, new PhoneDirectory("test2", "1234"));
			System.out.println(obj.findTable(conn, new PhoneDirectory("test2", "1234")));
			obj.SelectAll(conn);
			System.out.println(obj.getCount(conn));
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
