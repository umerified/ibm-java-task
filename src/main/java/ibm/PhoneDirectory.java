package ibm;

import java.io.Serializable;

public class PhoneDirectory implements Serializable {
	private static final long serialVersionUID = 1L;
	private String name;
	private String number;

	
	// ------------ Constructor start -------------
	public PhoneDirectory(String name, String number) {
		this.name = name;
		this.number = number;
	}
	// ------------ Constructor end -------------


	// ------------ Getters start -------------
	public String getName() {
		return name;
	}
	
	public String getNumber() {
		return number;
	}
	// ------------ Getters end -------------
	

	// ------------ Setters start -------------
	public void setName(String name) {
		this.name = name;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	// ------------ Setters end -------------

}
