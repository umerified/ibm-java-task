package ibm;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;


import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import net.minidev.json.JSONObject;

@RestController
@RequestMapping("/json")
public class PhoneDirectoryController {

	@RequestMapping(method=RequestMethod.GET, value="/corpusData")
    public JSONObject[] corpusData() {
    	JSONObject[] directories = null;
    	DB db = new DB();
		try {
			Connection conn = db.getConnection();
            directories = new JSONObject[db.getCount(conn)];
			ResultSet rs = db.SelectAll(conn); // get all the records

			for (int i=0; rs.next(); i++){ // making JSON of each record
    			directories[i] = new JSONObject();
    			directories[i].put("id", rs.getInt("ID"));
    			directories[i].put("name", rs.getString("NAME"));
    			directories[i].put("number", rs.getString("NUMBER"));
			}

			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
        	directories = new JSONObject[1];
        	directories[0] = new JSONObject();
            directories[0].put("message", "failure");
		}
        return directories;
    }

    @RequestMapping(method=RequestMethod.POST, value="/corpusUpdate")
    public JSONObject corpusUpdate(@RequestBody JSONObject data) throws Exception {
    	System.out.println(data);
        JSONObject returnMessage = new JSONObject();

    	if (data.getAsString("name") == null || data.getAsString("number") == null){ // validating the input
            returnMessage.put("message", "failure");
            return returnMessage;
        }
    	PhoneDirectory record = new PhoneDirectory(data.getAsString("name"), data.getAsString("number"));

    	DB db = new DB();
		try {
			Connection conn = db.getConnection();
			if (db.findTable(conn, record)){ // if the record already exists then update the record
				db.updateTable(conn, record);
	            returnMessage.put("message", "updated successfully");
			} else { // Otherwise add the new record
				db.insertTable(conn, record);
	            returnMessage.put("message", "added successfully");
			}
			conn.close();

        } catch(Exception e) {
        	e.printStackTrace();
            returnMessage.put("message", "failure");
        } 
        return returnMessage;
        
    }
}
